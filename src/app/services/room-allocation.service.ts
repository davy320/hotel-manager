import { Injectable } from '@angular/core';
import { Customer } from '../models/customer';
import { RoomType } from '../models/room';
import { Hotel } from '../models/hotel.model';

@Injectable({
  providedIn: 'root'
})
export class RoomAllocationService {

  constructor() { }
  return;

  // This function calculates the number of Economy rooms to be allocated and the remaining as business
  allocateRooms(availableBusinessRooms: number, availableEconomyRooms: number, customers: Customer[]): Hotel {
    customers.sort((a, b) => (a.priceOffer < b.priceOffer) ? 1 : -1);
    console.log('customers', customers);
    const indexAtMark = customers.findIndex(customer => customer.priceOffer < 100);
    const businessRoomsRequired = customers.slice(0, indexAtMark);
    const economyRoomsRequired = customers.slice(indexAtMark);
    console.log(businessRoomsRequired);
    console.log(economyRoomsRequired);
    let businessRoomsLeft = availableBusinessRooms;
    let economyRoomsLeft = availableEconomyRooms;
    let businessRoomsRevenue = 0;
    let economyRoomsRevenue = 0;
    businessRoomsRequired.forEach(room => {
      if (businessRoomsLeft > 0) {
        businessRoomsLeft -= 1;
        room.allocatedRoom = RoomType.business;
        businessRoomsRevenue += room.priceOffer;
      }
    });

    let economyRoomsShort = economyRoomsRequired.length - economyRoomsLeft;
    while (economyRoomsShort > 0 && businessRoomsLeft > 0) {
      businessRoomsLeft -= 1;
      const shiftedRoom = economyRoomsRequired.shift();
      shiftedRoom.allocatedRoom = RoomType.business;
      economyRoomsShort -= 1;
      businessRoomsRevenue += shiftedRoom.priceOffer;
    }
    economyRoomsRequired.forEach(room => {
      if (economyRoomsLeft > 0) {
        economyRoomsLeft -= 1;
        room.allocatedRoom = RoomType.economy;
        economyRoomsRevenue += room.priceOffer;

      }
    });


    console.log('Final business rooms', businessRoomsRequired);

    return {
      allocatedBusinessRooms: availableBusinessRooms,
      allocatedEconomyRooms: availableEconomyRooms,
      businessRoomsRevenue,
      economyRoomsRevenue
    };
  }

  clearAllocations(customers: Customer[]) {
    customers.forEach(customer => delete customer.allocatedRoom);
    console.log('delted after changes', customers);
    return customers;
  }
}
