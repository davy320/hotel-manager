import { TestBed } from '@angular/core/testing';

import { RoomAllocationService } from './room-allocation.service';
import { defaultCustomers } from '../../assets/default-inputs/defaultCustomers.json';
import { Customer } from '../models/customer';
import { FormBuilder } from '@angular/forms';

describe('RoomAllocationService', () => {
  let service: RoomAllocationService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(RoomAllocationService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});

describe('AllocatedRoomsTest', () => {
  let service: RoomAllocationService;
  const customers = [];
  defaultCustomers.forEach(customer => {
    customers.push({priceOffer: customer});
  });
  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(RoomAllocationService);
    TestBed.inject(FormBuilder);
  });

  it('Check customer price import', () => {
    console.log(service.allocateRooms(3, 3, customers));
    console.log(service.allocateRooms(7, 5, customers));
    console.log(service.allocateRooms(2, 7, customers));
    console.log(service.allocateRooms(7, 1, customers));
  });
});
