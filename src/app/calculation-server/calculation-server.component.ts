import { Component, OnInit, OnChanges, SimpleChanges } from '@angular/core';
import { defaultCustomers } from '../../assets/default-inputs/defaultCustomers.json';
import { Customer } from '../models/customer';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Hotel } from '../models/hotel.model';
import { RoomAllocationService } from '../services/room-allocation.service';

@Component({
  selector: 'app-calculation-server',
  templateUrl: './calculation-server.component.html',
  styleUrls: ['./calculation-server.component.scss']
})

export class CalculationServerComponent implements OnInit {
  customers: Customer[];

  availableEconomyRooms: number;
  availableBusinessRooms: number;

  hotel: Hotel;

  roomsForm: FormGroup;
  constructor(private formBuilder: FormBuilder, private roomAllocationsService: RoomAllocationService) {
    this.roomsForm = this.formBuilder.group({
      economyRooms: [0, Validators.required],
      businessRooms: [0, Validators.required]
    });
  }

  ngOnInit(): void {
    this.customers = [];
    defaultCustomers.forEach(customer => {
      this.customers.push({priceOffer: customer});
    });
    console.log(this.customers);
  }

  saveRooms(availableBusinessRooms: number, availableEconomyRooms: number) {
    this.availableBusinessRooms = availableBusinessRooms;
    this.availableEconomyRooms = availableEconomyRooms;
  }

  onSubmit(roomsForm: { businessRooms: number; economyRooms: number; }) {
    // Process checkout data here
    this.availableBusinessRooms = Number(roomsForm.businessRooms);
    this.availableEconomyRooms = Number(roomsForm.economyRooms);
    this.roomAllocationsService.clearAllocations(this.customers);
    console.log(this.availableBusinessRooms, this.availableEconomyRooms);
  }

  performAllocations() {
    console.log('Performing allocations');
    this.hotel = this.roomAllocationsService.allocateRooms(this.availableBusinessRooms, this.availableEconomyRooms, this.customers);
  }

}
