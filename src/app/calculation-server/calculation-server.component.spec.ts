import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CalculationServerComponent } from './calculation-server.component';

describe('CalculationServerComponent', () => {
  let component: CalculationServerComponent;
  let fixture: ComponentFixture<CalculationServerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CalculationServerComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CalculationServerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
