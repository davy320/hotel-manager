import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CalculationServerComponent } from './calculation-server/calculation-server.component';
import { WelcomePageComponent } from './welcome-page/welcome-page.component';


const routes: Routes = [
  {
    path: '',   redirectTo: '/welcome', pathMatch: 'full',
  },
  {
    path: 'welcome',
    component: WelcomePageComponent
  },
  {
    path: 'calc', component: CalculationServerComponent
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
