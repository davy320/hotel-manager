import { RoomType } from './room';

export interface Customer {
    allocatedRoom?: RoomType;
    priceOffer: number;
}
