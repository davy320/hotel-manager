export interface Hotel {
    allocatedEconomyRooms: number;
    allocatedBusinessRooms: number;
    businessRoomsRevenue: number;
    economyRoomsRevenue: number;
}
