export interface Room {
    type: RoomType;
}

// 2 types of rooms currently available
export enum RoomType {
    business = 'business',
    economy = 'economy'
}
